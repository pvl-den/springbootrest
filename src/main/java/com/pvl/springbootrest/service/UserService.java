package com.pvl.springbootrest.service;

import com.pvl.springbootrest.entity.User;

import java.util.List;

public interface UserService {

    User addUser (User user);
    void deleteUser(long id);
    User updateUser(User user);
    List<User> getAllUser();
    User findByLoginAndPassword (String login, String password);
}
