package com.pvl.springbootrest.service.impl;

import com.pvl.springbootrest.Repositories.UserRepository;
import com.pvl.springbootrest.entity.User;
import com.pvl.springbootrest.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        log.debug("Добавлен пользователь " + user);
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(long id) {
        log.debug("Удален пользователь с id: " + id);
        userRepository.delete(id);
    }

    @Override
    public User updateUser(User user) {
        log.debug("Изменен пользователь " + user);
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUser() {
        log.debug("Найдены все пользователи");
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User findByLoginAndPassword(String login, String password) {
        log.debug("Найден пользователь " + userRepository.findByLoginAndPassword(login,password));
        return userRepository.findByLoginAndPassword(login,password);
    }
}
