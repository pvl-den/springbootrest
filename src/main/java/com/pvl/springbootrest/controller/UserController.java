package com.pvl.springbootrest.controller;

import com.pvl.springbootrest.entity.User;
import com.pvl.springbootrest.service.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Data

@RestController
public class UserController {



    @Autowired
    private UserService userService;

    //найти всех пользователей
    @RequestMapping(value = "users", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getListUser(){
        return userService.getAllUser();
    }

    //найти пользователя по логину и паролю
    @RequestMapping(value = "user/{login}/{password}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable String login, @PathVariable String password){
        return userService.findByLoginAndPassword(login,password);
    }


    //добавить нового пользователя
    @RequestMapping(value = "user", method = RequestMethod.POST)
    @ResponseBody
    public User addUser(User user){
        return userService.addUser(user);
    }

    //изменить/заблокировать пользователя
    @RequestMapping(value = "user", method = RequestMethod.PUT)
    @ResponseBody
    public User updateUser(User user){
        return userService.updateUser(user);
    }

    //удалить пользователя
    @RequestMapping(value = "user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public void deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
    }




}
