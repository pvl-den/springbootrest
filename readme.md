 Разработать standalone app
=============================

Средства разработки: Java 1.8  
Framework: Spring boot 1.5.7  
База данных: H2 database in memory     
Протокол: HTTP, порт 8080

Среда разработки: IntelliJ IDEA 2017.2.3  
Браузер: Firefox

— Основной фреймворк приложения Spring Boot  
— maven  
— REST  
— Логирование: Logback

Cистема классов 
------------

//main class  
public class SpringbootrestApplication.java 

//Entity  
public class User 

//Repository  
public interface UserRepository  


//Service  
public interface UserService  
public class UserServiceImpl  


//RestController  
public class UserController

 

СПИСОК ЗАПРОСОВ
---------------
первоначальное заполнение БД  
INSERT INTO USER VALUES (1, 'TRUE', 'mylog', 'mypass');  
INSERT INTO USER VALUES (2, 'FALSE', 'denis', '123');

//вывести всех пользователей  
http://localhost:8080/users  

//найти пользователя по логину и паролю  
http://localhost:8080/user/denis/123  



возможные решения при масштабировании
---------------
- Вынесение базы данных на отдельный сервер
- Отделение Web сервера
- Подключение серверов кэширования
- Репликация
- Шардинг

-------------------------------------------------------
pvl-den@yandex.ru

